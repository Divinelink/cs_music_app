package com.example.estelle.cs_akazoo_app.features.playlists;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.estelle.cs_akazoo_app.R;
import com.example.estelle.cs_akazoo_app.features.tracks.TracksActivity;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlaylistsFragment extends Fragment implements PlaylistsView {


    RecyclerView playlistsRv;
    EditText mFilterEditText;
    Button mFilterButton;


    PlaylistsPresenter presenter;

    public PlaylistsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_playlists, container, false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        presenter = new PlaylistsPresenterImpl(this);

        playlistsRv = v.findViewById(R.id.playslists_rv);
        playlistsRv.setLayoutManager(layoutManager);

        mFilterEditText = v.findViewById(R.id.filter_edit_text);
        mFilterButton = v.findViewById(R.id.filter_button);

        mFilterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String filterText =  mFilterEditText.getText().toString();
                presenter.getFilteredPlaylists(filterText);
            }
        });


        presenter.getPlaylists();

        return v;
    }


    @Override
    public void showPlaylists(ArrayList<PlaylistUI> playlists)
    {
        PlaylistsRvAdapter playlistsRvAdapter = new PlaylistsRvAdapter(playlists, new OnPlaylistClickListener() {
            @Override
            public void onPlaylistClicked(PlaylistUI playlist) {

                Intent intent = new Intent(getActivity(), TracksActivity.class);
                intent.putExtra("playlistId", playlist.getPlaylistId());
                startActivity(intent);

            }
        }, getActivity()
        );
        playlistsRv.setAdapter(playlistsRvAdapter);
    }

    @Override
    public void showGeneralError() {

        Toast.makeText(
                getActivity(),
                getResources().getString(R.string.general_error_message),
                Toast.LENGTH_SHORT)
                .show();
    }

}
