package com.example.estelle.cs_akazoo_app.features.tracks;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.estelle.cs_akazoo_app.R;
import com.example.estelle.cs_akazoo_app.features.playlists.PlaylistUI;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class TracksFragment extends Fragment implements TracksView{

    RecyclerView mTracksRv;
    TracksPresenter presenter;

    @BindView(R.id.filter_edit_text_tracks) EditText mFilterEditText;
    @OnClick(R.id.filter_button_tracks) void submit() {

        String filterText =  mFilterEditText.getText().toString();
        presenter.getFilteredTracks(filterText);

    }

    public TracksFragment() {
    }
    public static TracksFragment newInstance(PlaylistUI playlist)
    {
        TracksFragment myFragment = new TracksFragment();
        Bundle args = new Bundle();
        args.putParcelable("playlist", playlist);
        myFragment.setArguments(args);

        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        PlaylistUI playlist = getArguments().getParcelable("playlist");

        View v = inflater.inflate(R.layout.fragment_tracks, container, false);

        ButterKnife.bind(this, v);

        mTracksRv = v.findViewById(R.id.tracks_rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mTracksRv.setLayoutManager(layoutManager);
        

        presenter = new TracksPresenterImpl(this);

        presenter.getTracks();

        return v;
    }


    @Override
    public void showTracks(ArrayList<TrackUI> tracks)
    {
        TracksRvAdapter tracksRvAdapter = new TracksRvAdapter(tracks, new OnTrackClickListener() {
            @Override
            public void onTrackClick(TrackUI track) {
                Toast.makeText(getActivity(), "The song "
                        + track.getTrackName() + " got clicked", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTrackLogoClick(TrackUI track) {
                Toast.makeText(getActivity(), "The track logo got clicked", Toast.LENGTH_SHORT).show();
            }
        }, getActivity());
        mTracksRv.setAdapter(tracksRvAdapter);
    }


}
