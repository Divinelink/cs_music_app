package com.example.estelle.cs_akazoo_app.features.playlists;

import com.example.estelle.cs_akazoo_app.rest.responses.PlaylistsResponse;

import java.util.ArrayList;

public interface PlaylistsInteractor {

    // Βάζουμε void γιατί αλλιώς θα περίμενε να επιστρέψει κάποιο
    // αποτέλεσμα, οπότε ουσιαστικά η μέθοδος θα γινόταν σύγχρονη.
    void getPlaylists(OnPlaylistsFinishListener listener);

    void getFilteredPlaylists(OnPlaylistsFinishListener listener, String filterString);

    interface OnPlaylistsFinishListener
    {
        // O Interactor δεν θέλει να δέχεται PlaylistUI, αλλά PlaylistDomain.
        void onSuccess(ArrayList<PlaylistDomain> playlists);

        void onError();
    }
}
