package com.example.estelle.cs_akazoo_app.features.tracks;

import java.util.ArrayList;

public interface TracksInteractor {

    void getTracks(OnTrackFinishListener listener);

    void getFilteredTracks(OnTrackFinishListener listener, String filterString);

    interface OnTrackFinishListener{

        // O Interactor δεν θέλει να δέχεται TracksUI, αλλά TracksDomain.
        void onSuccess(ArrayList<TrackDomain> arrayList);

        void onError();
    }
}
