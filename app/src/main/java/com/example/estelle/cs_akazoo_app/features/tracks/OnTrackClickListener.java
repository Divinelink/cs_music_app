package com.example.estelle.cs_akazoo_app.features.tracks;

public interface OnTrackClickListener {

    void onTrackClick(TrackUI track);

    void onTrackLogoClick(TrackUI track);

}
