package com.example.estelle.cs_akazoo_app.features.playlists;

import java.util.ArrayList;

public interface PlaylistsPresenter {

    // Interfaces είναι τα συμβόλαια ανάμεσα στα layers.

    void getPlaylists();

    void getFilteredPlaylists(String filter);

}
