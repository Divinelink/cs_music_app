package com.example.estelle.cs_akazoo_app.features.playlists;

public interface OnPlaylistClickListener {

    void onPlaylistClicked(PlaylistUI playlist);

}
