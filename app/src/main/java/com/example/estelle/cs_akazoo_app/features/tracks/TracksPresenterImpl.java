package com.example.estelle.cs_akazoo_app.features.tracks;

import com.example.estelle.cs_akazoo_app.R;

import java.util.ArrayList;

public class TracksPresenterImpl implements TracksPresenter, TracksInteractor.OnTrackFinishListener {

    private TracksView tracksView;

    private TracksInteractorImpl interactor;

    public TracksPresenterImpl(TracksView tracksView) {
        this.tracksView = tracksView;
        interactor = new TracksInteractorImpl();
    }

    @Override
    public void getTracks() {

        interactor.getTracks(this);

    }

    @Override
    public void getFilteredTracks(String filterString) {

        interactor.getFilteredTracks(this, filterString);
    }



    @Override
    public void onSuccess(ArrayList<TrackDomain> arrayList) {

        // Εδώ εκτελείται ή λογική του Presenter

        ArrayList<TrackUI> trackUI = new ArrayList<>();

        for (TrackDomain trackd : arrayList)
        {
            TrackUI tracksUI = new TrackUI(
                    trackd.getTrackName(),
                    trackd.getTrackArtist(),
                    trackd.getTrackCategory()
            );

            if (tracksUI.getTrackCategory().equals("pop") || tracksUI.getTrackCategory().equals("rock"))
                tracksUI.setTrackNameCOlorId(R.color.red);
            else
                tracksUI.setTrackNameCOlorId(R.color.blue);
            trackUI.add(tracksUI);
        }

        tracksView.showTracks(trackUI);
    }

    @Override
    public void onError() {

    }
}
