package com.example.estelle.cs_akazoo_app.rest;

import com.example.estelle.cs_akazoo_app.features.playlists.PlaylistDomain;
import com.example.estelle.cs_akazoo_app.rest.responses.PlaylistsResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RestAPI {

    @GET("playlists")
    Call<PlaylistsResponse> fetchPlaylists();

}
