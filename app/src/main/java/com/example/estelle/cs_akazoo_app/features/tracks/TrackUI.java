package com.example.estelle.cs_akazoo_app.features.tracks;

public class TrackUI {

    // Το TrackUI πρέπει να έχει όσα properties ενδέχεται να αλλάξει ο Presenter.

    private String trackName;
    private String trackArtist;
    private String trackCategory;
    private String trackLogoUrl;
    private int trackNameCOlorId;

    public TrackUI(String trackName, String trackArtist, String trackCategory) {
        this.trackName = trackName;
        this.trackArtist = trackArtist;
        this.trackCategory = trackCategory;
    }

    public TrackUI(String trackName, String trackArtist, String trackCategory, String trackLogoUrl) {
        this.trackName = trackName;
        this.trackArtist = trackArtist;
        this.trackCategory = trackCategory;
        this.trackLogoUrl = trackLogoUrl;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getTrackArtist() {
        return trackArtist;
    }

    public void setTrackArtist(String trackArtist) {
        this.trackArtist = trackArtist;
    }

    public String getTrackCategory() {
        return trackCategory;
    }

    public void setTrackCategory(String trackCategory) {
        this.trackCategory = trackCategory;
    }

    public String getTrackLogoUrl() {
        return trackLogoUrl;
    }

    public void setTrackLogoUrl(String trackLogoUrl) {
        this.trackLogoUrl = trackLogoUrl;
    }

    public int getTrackNameCOlorId() {
        return trackNameCOlorId;
    }

    public void setTrackNameCOlorId(int trackNameCOlorId) {
        this.trackNameCOlorId = trackNameCOlorId;
    }
}
