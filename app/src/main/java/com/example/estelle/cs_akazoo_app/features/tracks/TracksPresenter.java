package com.example.estelle.cs_akazoo_app.features.tracks;

public interface TracksPresenter {

    void getTracks();

    void getFilteredTracks(String filterString);

}
