package com.example.estelle.cs_akazoo_app.features.tracks;


import java.util.ArrayList;

public class TracksInteractorImpl implements TracksInteractor {

    @Override
    public void getTracks(OnTrackFinishListener listener) {

        ArrayList<TrackDomain> tracks = getMockedTracks();

        listener.onSuccess(tracks);
    }

    @Override
    public void getFilteredTracks(OnTrackFinishListener listener, String filterString) {

        ArrayList<TrackDomain> filteredTracks = new ArrayList<>();
        ArrayList<TrackDomain> tracks = getMockedTracks();

        for (int i = 0; i < tracks.size(); i++) {
            if (tracks.get(i).getTrackName().startsWith(filterString))
                filteredTracks.add(tracks.get(i));
        }

        listener.onSuccess(filteredTracks);

    }

    private ArrayList getMockedTracks() {
        ArrayList<TrackDomain> tracks = new ArrayList<>();

        for (int i = 1; i < 100; i++) {
            tracks.add(new TrackDomain("name", "artist", "pop"));
            tracks.add(new TrackDomain("name2", "artist2", "pop"));
            tracks.add(new TrackDomain("name3", "artist3", "metal"));
            tracks.add(new TrackDomain("name4", "artist4", "rock"));
            tracks.add(new TrackDomain("name5", "artist5", "rock"));
            tracks.add(new TrackDomain("name6", "artist6", "punk"));
            tracks.add(new TrackDomain("name7", "artist7", "rock"));
            tracks.add(new TrackDomain("name8", "artist8", "metal"));
            tracks.add(new TrackDomain("name9", "artist9", "pop"));
            tracks.add(new TrackDomain("name10", "artist10", "rock"));
        }

        return tracks;
    }

}
