package com.example.estelle.cs_akazoo_app.features.tracks;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.estelle.cs_akazoo_app.R;

public class TracksActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracks);

        String playlistId = getIntent().getStringExtra("playlistId");
        Log.v("CS_TAG", "The playlistId: " + playlistId);


        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.tracks_root, new TracksFragment())
                .commit();
    }
}
